# Muscle Joystick

## Brief

This project is an experiement in simple human-machine interfaces. It allows you to turn two muscles in your body into a human input device that you can connect to a computer.

## Implementation

This project uses an Arduino Leonardo to communication with a pair of SparkFun [v3 Muscle Sensors](https://www.sparkfun.com/products/13027 "Muscle Sensor").

The Leo has built-in USB capability, and can be programmed to appear as a number of HID devices over USB. In this experiment we're making the Leo appear as a joystick.

We connect each muscle sensor to a different muscle. One sensor controls the upward motion, and another muscle controls the downward motion. The sensor values are added together to get the joystick position. For this reason, it's a good idea to choose two muscles in opposition to each other, like a flexor and a matching extensor.

An auto-calibration routine allows the sensors to be installed on any muscle on any person between uses without rewriting any code.

## Source

### MuscleSensorTest.ino

This file contains a simple LED demo of the muscle sensor and the auto-calibration routine