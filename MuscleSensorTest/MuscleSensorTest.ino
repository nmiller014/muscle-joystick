#include <Adafruit_NeoPixel.h>

#define NEOPIXEL_COUNT 31
#define NEOPIXEL_PIN 6
#define BUTTON_PIN (2)
#define MUSCLE_SENSOR_ANALOG_PIN (A5)
#define AVG_MEASUREMENT_COUNT (256)
#define MEASUREMENT_PERIOD_MS (3000)
#define MEASUREMENT_DELAY_MS (MEASUREMENT_PERIOD_MS/AVG_MEASUREMENT_COUNT)

void buttonIsr(void);
void clearStrip(void);
void calibrate(void);

int sensorValue = 0;
unsigned int brightness = 0;

unsigned int relaxedMeasurement;
unsigned int tenseMeasurement;

bool recalibrate = false;

Adafruit_NeoPixel strip = Adafruit_NeoPixel(NEOPIXEL_COUNT, NEOPIXEL_PIN, NEO_GRB | NEO_KHZ800);

void setup()
{
  strip.begin();
  strip.show(); // Initialize all pixels to 'off'

  Serial.begin(9600);

  calibrate();

  pinMode(BUTTON_PIN, INPUT);
  attachInterrupt(digitalPinToInterrupt(BUTTON_PIN), buttonIsr, RISING);
  interrupts();
}

void loop()
{
  if (recalibrate)
  {
    Serial.println("Recalibrating");
    calibrate();
    recalibrate = false;
  }

  Serial.print("button = " );
  Serial.println(digitalRead(BUTTON_PIN));

  // read the analog in value:
  sensorValue = analogRead(MUSCLE_SENSOR_ANALOG_PIN);
  
  Serial.print("(");
  Serial.print(relaxedMeasurement);
  Serial.print(",");
  Serial.print(tenseMeasurement);
  Serial.println(")");

  // print the results to the serial monitor:
  Serial.print("sensor = " );
  Serial.println(sensorValue);
  
  if (sensorValue < relaxedMeasurement)
  {
    sensorValue = relaxedMeasurement;
  }
  
  brightness = map(sensorValue, relaxedMeasurement, tenseMeasurement, 0, NEOPIXEL_COUNT);
  
  Serial.print("brightness = ");
  Serial.println(brightness);
  Serial.println("");
  
  int i;
  for (i = 0; i < NEOPIXEL_COUNT; i++)
  {
    strip.setPixelColor(i, 0, 0, 0);
  }
  
  for (i = 0; i < brightness; i++)
  {
    strip.setPixelColor(i, 0, 255, 0);
  }
  
  strip.show();

  delay(75);
}

void clearStrip(void)
{
  int i;
  for (i = 0; i < NEOPIXEL_COUNT; i++)
  {
    strip.setPixelColor(i, 0, 0, 0);
  }
  strip.show();
}


void calibrate(void)
{
  uint32_t sum;
  int i;
  int startTime;
  int tmp;

  clearStrip();
  
  Serial.println("Relax your muscle while the light is green");
  
  for (i = 0; i < NEOPIXEL_COUNT; i++)
  {
    strip.setPixelColor(i, 0, 0, 255);
  }
  strip.show();
  
  for (i = 0; i < AVG_MEASUREMENT_COUNT; i++)
  {
    tmp = analogRead(MUSCLE_SENSOR_ANALOG_PIN);
    Serial.println(tmp);
    sum += tmp;
    delay(MEASUREMENT_DELAY_MS);
  }
  
  relaxedMeasurement = ((sum*1000) / AVG_MEASUREMENT_COUNT)/1000;
  
  Serial.println("Tense your muscle as hard as you can while the light is red!");
  
  for (i = 0; i < NEOPIXEL_COUNT; i++)
  {
    strip.setPixelColor(i, 255, 0, 0);
  }
  strip.show();
  
  tenseMeasurement = 0;
  startTime = millis();
  
  while ((unsigned long)(millis() - startTime) >= MEASUREMENT_PERIOD_MS)
  {
    tmp = analogRead(MUSCLE_SENSOR_ANALOG_PIN);
    if (tmp > tenseMeasurement)
    {
      tenseMeasurement = tmp;
      Serial.println(tmp);
    }
    delay(2);
  }
  
  Serial.print("Relaxed measurement = ");
  Serial.println(relaxedMeasurement);
  
  Serial.print("Tense measurement = ");
  Serial.println(tenseMeasurement);
  
  delay(1000);
}

void buttonIsr(void)
{
  recalibrate = true;
}